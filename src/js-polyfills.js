import Svgxuse from 'svgxuse'
import ArrayFrom from './array-from'
import ObjectFitPolyfill from 'objectFitPolyfill'
import ForEach from './forEach'
import Includes from './includes'
import Append from './append'
import Preppend from './preppend'
import CustomEvent from './custom-event'
import Matches from './matches'
import Closest from './closest'
import Slice from './slice'
import Promise from './promise'
import Fetch from './fetch'
import Micromodal from './micromodal'
import ObjectAssign from './object-assign'
import Remove from './remove'

export {
    Append,
    Closest,
    CustomEvent,
    ForEach,
    Includes,
    Matches,
    Preppend,
    Slice,
    Svgxuse,
    Promise,
    Fetch,
    Micromodal,
    ObjectAssign,
    ObjectFitPolyfill,
    Remove,
    ArrayFrom
}
